import argparse
from geolocation import Geolocation


def main(filename: str, latitude: float, longitude: float) -> None:
    geolocation = Geolocation(filename)
    print(geolocation.search(latitude, longitude))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset_filename', type=str)
    parser.add_argument('latitude', type=float, help='latitude')
    parser.add_argument('longitude', type=float, help='longitude')
    args = parser.parse_args()
    main(args.dataset_filename, args.latitude, args.longitude)