from abc import abstractmethod
from typing import Optional

import geojson
import numba
from numba import jit


class AbstractGeolocation:
    @abstractmethod
    def search(self, latitude: float, logitude: float):
        """

        :param latitude:
        :param logitude:
        :return: the city that contains (latitude, longitude)
        """


def _point_in_polygon(x, y, poly):
    n = len(poly)
    inside = False
    p2x = 0.0
    p2y = 0.0
    xints = 0.0
    p1x, p1y = poly[0]
    for i in numba.prange(n + 1):

        p2x, p2y = poly[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xints = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x, p1y = p2x, p2y

    return inside


class Geolocation(AbstractGeolocation):
    def __init__(self, filename: str):
        with open(filename, "r") as fh:
            self.data = geojson.load(fh)

    def search(self, latitude: float, longitude: float) -> Optional[str]:
        for item in self.data['features']:
            if item['geometry']['type'] == 'Polygon':
                if _point_in_polygon(longitude, latitude, item['geometry']['coordinates'][0]):
                    return item['properties']
            elif item['geometry']['type'] == 'MultiPolygon':
                if _point_in_polygon(longitude, latitude, item['geometry']['coordinates'][0][0]):
                    return item['properties']
            else:
                print('invalid type', item['geometry']['type'])
                return None
        else:
            return None
