import argparse
from flask import Flask, jsonify
from geolocation import Geolocation


# geolocation = Geolocation("data/cities.json")
geolocation = Geolocation("data/uf.json")


app = Flask('geolocation')


@app.route("/search/<latitude>,<longitude>", methods=["GET"])
def search(latitude: str, longitude: str):
    return jsonify({'city': geolocation.search(float(latitude), float(longitude))})


@app.route('/say/<nome>')
def say(nome):
    return nome


@app.route('/')
def index():
    return jsonify({'result': 'ok'})


if __name__ == "__main__":
    app.run(debug=True)
