# Installation

1. Install Python 3.7+
2. Create a virtual environment and install the requirements:
```shell
virtualenv --python=python3.8 venv
source venv/bin/activate
pip install -r requirements/base.txt
```

# Running

Run the app:
```shell
python app.py
```

It should print something like this:

```shell
 * Serving Flask app 'geolocation' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 443-976-191
```

(It is running at 127.0.0.1, port 5000)

To test if the service is running properly, open a browser and access the following URL:

```
http://localhost:5000/search/-8.074846642726218,-35.36059422566669
```

It should return a json:

```json
{
  "city": {
    "GEOCODIGO": "26", 
    "MESO": "AGRESTE PERNAMBUCANO", 
    "MICRO": "ALTO CAPIBARIBE", 
    "NOME_UF": "Pernambuco", 
    "REGIAO": "Nordeste", 
    "UF_05": "PE"
  }
}
```